#!/bin/bash

# shellcheck disable=SC2086

echo -e "\n[INFO] Checking effective user id.";
if [[ $EUID -ne 0 ]]; then
    echo -e "\n[FAIL] Script must be run with sudo or as root; exiting." 1>&2;
    exit 1;
else
    echo -e "\n[INFO] Effective user id is zero (i.e. root or sudo user); proceeding with install.";
fi;

# Script's system package dependencies
DEPS="python2.7 tar coreutils wget binutils libatk-bridge2.0 libgtk-3-0 libxss1 findutils openssl unzip"

if [[ "$1" != "-y" ]]; then
    echo -e "\n[INFO] Required dependencies include: $DEPS"
    read -rp "[INFO] Would you like this script to automatically globally install the required dependencies? [Y/n] ";
    if [[ $REPLY == "Y" ]]; then
        echo -e "\n[INFO] Installing dependencies."
        sudo apt-get install -y $DEPS;
    else
        echo -e "\n[WARN] Not installing dependencies; failure to install dependencies manually will result in a failed installation." 1>&2;
    fi;
else
    echo -e "\n[INFO] Installing dependencies."
    sudo apt-get install -y $DEPS;
fi; 

# Default cackey library location
CACKEY_LOC="/usr/lib64/libcackey.so"

# Default VMWare Horizon client coolkey library location
VMWARE_COOLKEY_LOC="/usr/lib/vmware/view/pkcs11/libcoolkeypk11.so"

echo -e "\n[INFO] Testing if cackey already installed.";
if test -f "$CACKEY_LOC"; then
    echo -e "\n[INFO] cackey is already installed; skipping installation.";
else
    echo -e "\n[INFO] cackey is not already installed; installing.";
    sudo apt-get install -y pcsc-tools pcscd && \
    sudo mkdir -p /tmp/cackey/ && \
    (
      cd /tmp/cackey/ && \
      sudo rm -fr /tmp/cackey/* && \
      sudo wget http://cackey.rkeene.org/download/0.7.5/cackey-0.7.5-x86_64-1.tgz && \
      sudo tar -xf cackey* && \
      sudo mkdir -p /usr/lib64/ && \
      sudo cp usr/lib64/libcackey* /usr/lib64/ && \
      rm -fr /tmp/cackey/;
    ) 
    if test -f $CACKEY_LOC; then
        echo -e "\n[INFO] Successfully install cackey.";
    else
        echo -e "\n[FAIL] Failed to install cackey; exiting." 1>&2;
        exit 1;
    fi; 
fi;

echo -e "\n[INFO] Checking if VMWare Horizon client is already installed.";
if hash vmware-view 2>/dev/null; then
    echo -e "\n[INFO] VMWare Horizon client is already installed; skipping installation.";
else
    echo -e "\n[INFO] VMWare Horizon client is not already installed; installing.";
    if ! test -f horizon-5.3.0.bundle; then
    	wget -O horizon-5.3.0.bundle https://download3.vmware.com/software/view/viewclients/CART20FQ4/VMware-Horizon-Client-5.3.0-15208949.x64.bundle
    fi;
    chmod u+x horizon-5.3.0.bundle && \
    sudo TERM=dumb VMWARE_EULAS_AGREED=yes \
    ./horizon-5.3.0.bundle  --console --required \
    --set-setting vmware-horizon-smartcardsmartcardEnable yes \
    --set-setting vmware-horizon-rtavrtavEnable yes \
    --set-setting vmware-horizon-virtual-printing tpEnable yes \
    --set-setting vmware-horizon-tsdrtsdrEnable yes \
    --set-setting vmware-horizon-mmr mmrEnableyes \
    --set-setting vmware-horizon-media-provider mediaproviderEnable yes;
   if hash vmware-view 2>/dev/null; then
       echo -e "\n[INFO] Successfully installed VMware Horizon client.";
   else
       echo -e "\n[FAIL] Failed to install VMWare Horizon client." 1>&2;
       exit 1;
   fi;       
fi;

echo -e "\n[INFO] Replacing VMWare Horizon's default PKI library (coolkey) with cackey.";
{ sudo mkdir -p "/usr/lib/vmware/view/pkcs11/" && \
  declare RANDO_DIR"=$RANDOM" && \
  sudo mkdir -p "/tmp/$RANDO_DIR" && \
  if [[ -e "$VMWARE_COOLKEY_LOC" ]]; then
      echo -e "\n[INFO] Moving VMWare Horizon's default PKI library (coolkey) from current location ($VMWARE_COOLKEY_LOC) to temp dir (/tmp/$RANDO_DIR/coolkey.so)." && \
      sudo mv "$VMWARE_COOLKEY_LOC" "/tmp/$RANDO_DIR/coolkey.so";
  fi;
  echo -e "\n[INFO] Symlinking cackey into default coolkey location ($VMWARE_COOLKEY_LOC)." && \
  sudo ln -s "$CACKEY_LOC" "$VMWARE_COOLKEY_LOC" && \
  sudo ls -hal "$VMWARE_COOLKEY_LOC" && \
  echo -e "\n[INFO] Successfully replaced coolkey with cackey.";
} || \
{ echo -e "\n[FAIL] Failed to successfully replace coolkey with cackey; exiting."; \
  exit 1; }

echo -e "\n[INFO] Adding DoD certificates to CA certificate trust.";
{ wget -q https://militarycac.com/maccerts/AllCerts.zip && \
  sudo unzip AllCerts.zip -d CERTS && \
  sudo find . -name \*.cer -execdir openssl x509 -in "{}" -inform DER -out /usr/local/share/ca-certificates/"{}".crt \; && \
  sudo update-ca-certificates && \
  echo -e "\n[INFO] Successfully added DoD certificates to CA certificate trust.";
} || \
{ echo -e "\n[FAIL] Failed to add DoD certificates to CA certificate trust; exiting." 1>&2;
  exit 1; };

echo -e "\n[INFO] Adding TLS 1.1 support to VMWare Horizon client configuration.";
{ sudo mkdir -p "$HOME/.vmware/" && \
  sudo chown -R "${SUDO_USER}":"${SUDO_USER}" "$HOME/.vmware/" && \
  echo "view.sslProtocolString = \"TLSv1.1\"" >> "$HOME/.vmware/view-preferences" && \
  sudo chown -R "${SUDO_USER}":"${SUDO_USER}" "$HOME/.vmware/view-preferences" && \
  echo -e "\n[INFO] Successfully added TLS 1.1 support to VMWare Horizon configuration."; } || \
{ echo -e "\n[FAIL] Failed to add TLS 1.1 support to VMWare Horizon configuration; exiting."; \
  exit 1; };

echo -e "\nInstallation complete! Launch Horizon and add the following server:";
echo -e "\nVDI Address: https://afrcdesktops.us.af.mil";

