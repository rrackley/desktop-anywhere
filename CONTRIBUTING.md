# CONTRIBUTING

## Installer

### Development

Using your favorite IDE, modify `ubuntu.sh` to fix bugs and add features.

### Testing

The `shellcheck` command is used to to lint the file. It already runs during the `docker build` process, but if you would like to test your shell syntax prior to the build stage, please use the following command:

```
shellcheck ubuntu.sh
```

Run a docker build to install the script in a test environment. If the build completes, the installer has passed a basic smoke test.

```
docker build -t da-installer-dev .
```

Once built, validate functionality with this `docker run` command:

```
docker run \
  --rm \
  -it \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v ${HOME}/.vmware:/root/.vmware/ \
  -v /etc/localtime:/etc/localtime:ro \
  -e DISPLAY=$DISPLAY \
  --user horizon \
  da-installer-dev \
  vmware-view;
```

Note: This docker run test does not check whether or not authentication is possible via smart card, just that `vmware-view` installed properly and is able to launch.
