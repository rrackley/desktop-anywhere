FROM ubuntu:18.04
RUN { echo "[INFO] Updating system package cache and installing dependencies." && \
      apt-get update && \
      apt-get install -y sudo bash shellcheck && \
      echo "[INFO] Adding horizon user." && \
      useradd -s /bin/bash horizon; }
ADD . /workdir
RUN { echo "[INFO] Running installer." && \
      cd /workdir && \
      shellcheck ubuntu.sh && \
      chmod u+x /workdir/ubuntu.sh && \
      /workdir/ubuntu.sh -y; }
